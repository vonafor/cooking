# coding=utf-8
import csv
from itertools import chain
import datetime
import pandas
import scipy
from sklearn import tree, ensemble, neural_network
import numpy as np
from sklearn.preprocessing import LabelEncoder


def prepare_data(data, fs):
    result = scipy.sparse.dok_matrix((len(data), len(fs)), dtype=np.dtype(bool))
    for d, dish in enumerate(data['ingredients']):
        for ingredient in dish:
            i = fs.get(ingredient)
            if i is not None:
                result[d, i] = True
    return result


start = datetime.datetime.now()

train_data = pandas.read_json('train.json')
test_data = pandas.read_json('test.json')

features = set(chain(*train_data['ingredients']))
features = {k: v for k, v in zip(features, range(len(features)))}


X_train = prepare_data(train_data, features)
X_test = prepare_data(test_data, features)


label = LabelEncoder()
Y_train = label.fit_transform(train_data['cuisine'])


#  Обучение
clf = tree.DecisionTreeClassifier()
clf = clf.fit(X_train, Y_train)
prediction = clf.predict(X_test)


with open('result.csv', 'w') as output:
    s = csv.writer(output)
    s.writerow(['id', 'cuisine'])
    for i, y in zip(test_data['id'], label.inverse_transform(prediction)):
        s.writerow([i, y])

print datetime.datetime.now() - start
